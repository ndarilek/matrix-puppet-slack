FROM node:9-alpine as builder
RUN apk update
RUN apk add git
RUN git clone https://github.com/ndarilek/matrix-puppet-slack /app
WORKDIR /app
RUN npm install

FROM node:9-alpine
COPY --from=builder /app /app
WORKDIR app
RUN adduser -DHS app
RUN chown -R app *
USER app
ENTRYPOINT ["node", "/app/index.js"]
